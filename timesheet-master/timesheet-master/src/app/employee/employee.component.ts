import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { environment } from 'src/environments/environment.prod';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls: ['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    empList: Number = 1;
    tasklist: any;
    total: any = {};
    selectedUser: any;
    selectedWeek: any;
    dayList: any = environment.days;
    weekList:any = environment.weeks;
    constructor(private employeeService: EmployeeService) { }

    ngOnInit() {
        this.getEmplployeeList();
        this.initiateTotal();
    }
    public initiateTotal() {
        for(let item of this.dayList){
            this.total[item] = 0;
        }
    }

    public getEmplployeeList() {
        this.employeeService.getallemployees().subscribe(data => {
            this.employees = data;
            this.selectedUser = data[0].id;
            this.selectedWeek = this.weekList[0]['id'];
        });
    }
    public ShowTimeSheet() {
        this.empList = 0;
        this.getTasklistInfo();
    }
    public ShowList() {
        this.empList = 1;
        this.getEmplployeeList();
    }

    public getTasklistInfo() {
        let userid = this.selectedUser;
        let weekid = this.selectedWeek;
        this.employeeService.getTaskList(userid, weekid).subscribe(data => {
            this.tasklist = data;
            this.total = this.makeTotal();
        });
    }

    public makeTotal() {
        let data = this.tasklist
        this.initiateTotal();
        let total: any = this.total;
        for (let item = 0; item < data.length; item++) {
            for (let key in data[item]) {
                total[key] = parseInt(total[key]) + parseInt(data[item][key]);
            }
        }
        return total;
    }

    public fetchTasks() {
        console.log("SELECTED USER : ", this.selectedUser, "SELECTED WEEK : ", this.selectedWeek);
        this.getTasklistInfo();
    }

    public SaveData() {
        this.employeeService.savetasklist(this.selectedUser,this.selectedWeek,this.tasklist).subscribe(data => {
            //
        });
    }

}