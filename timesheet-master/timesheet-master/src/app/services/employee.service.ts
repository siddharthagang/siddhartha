import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    //private baseapi = environment.apiUrl;
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(environment.apiUrl + environment.fetchAllEmployee);
    }
    getTaskList(userid, weekid) {
        return this.http.get(environment.apiUrl + environment.fetchTaskList +"/"+userid+"/"+weekid);
    }
    savetasklist(userid, weekid, tasks) {
        let sendables = {userid,weekid,tasks};
        return this.http.post(
            environment.apiUrl + environment.saveTaskList,
            sendables,
            this.httpOptions
        );
    }
}