export const environment = {
  production: true,
  apiUrl: "http://localhost:1475/api",

  fetchAllEmployee: "/values/getalltotal",
  fetchTaskList: "/values/gettaskwithtime",
  saveTaskList: "/values",
  days: [
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thrusday',
    'friday',
    'saturday'
  ],
  weeks: [
    {
      id: '1',
      displayName: 'WEEK-1'
    },
    {
      id: '2',
      displayName: 'WEEK-2'
    },
    {
      id: '3',
      displayName: 'WEEK-3'
    },
    {
      id: '4',
      displayName: 'WEEK-4'
    }
  ]

};
