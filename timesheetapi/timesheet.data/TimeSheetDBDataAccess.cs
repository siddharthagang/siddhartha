﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using timesheet.model;

namespace timesheet.data
{
   public  class TimeSheetDBDataAccess
    {
        string connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=TimeSheet;Data Source=INDLAPTOP280\SQLEXPRESS";

        public IEnumerable<Employee> GetAllEmployee()
        {
            try
            {
                List<Employee> lstEmployee = new List<Employee>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_GetALLEmployee", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Employee emp = new Employee();
                        emp.Id = Convert.ToInt32(rdr["Id"]);
                        emp.Code = Convert.ToString(rdr["Code"]);
                        emp.Name = Convert.ToString(rdr["Name"]);

                        lstEmployee.Add(emp);
                    }
                    con.Close();
                }
                return lstEmployee;
            }
            catch
            {
                throw;
            }
        }

        public Employee GetEmployeeByID(int EmployeeID)
        {
            try
            {

                Employee emp = new Employee();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_GetEmployeeByID", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ID", EmployeeID));
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        emp.Id = Convert.ToInt32(rdr["Id"]);
                        emp.Code = Convert.ToString(rdr["Code"]);
                        emp.Name = Convert.ToString(rdr["Name"]);
                    }
                    con.Close();
                }
                return emp;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<Employee> GetAllTimes()
        {
            try
            {
                List<Employee> lstEmployee = new List<Employee>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_GetALLEmployeeTimes", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Employee emp = new Employee();
                        emp.Id = Convert.ToInt32(rdr["Id"]);
                        emp.Code = Convert.ToString(rdr["Code"]);
                        emp.Name = Convert.ToString(rdr["Name"]);
                        emp.Total = Convert.ToDouble(rdr["Total"]);
                        emp.Avg = Convert.ToDouble(rdr["Avg"]);
                        lstEmployee.Add(emp);
                    }
                    con.Close();
                }
                return lstEmployee;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<EmpTaskTran> GetTaskWithTime(int Empid,int WeekID)
        {
            try
            {
                List<EmpTaskTran> lstEmployee = new List<EmpTaskTran>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_GetAllTaskEmployeeIDAndWeedID", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmpID", Empid));
                    cmd.Parameters.Add(new SqlParameter("@WeekID", WeekID));
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        EmpTaskTran emp = new EmpTaskTran();
                        emp.TaskId = Convert.ToInt32(rdr["TaskId"]);
                        emp.TaskName = Convert.ToString(rdr["TaskName"]);
                        emp.Sunday = Convert.ToInt32(rdr["sunday"]);
                        emp.Monday = Convert.ToInt32(rdr["monday"]);
                        emp.Tuesday = Convert.ToInt32(rdr["tuesday"]);
                        emp.Wednesday = Convert.ToInt32(rdr["wednesday"]);
                        emp.Thrusday = Convert.ToInt32(rdr["thrusday"]);

                        emp.Friday = Convert.ToInt32(rdr["friday"]);
                        emp.Saturday = Convert.ToInt32(rdr["saturday"]);


                        lstEmployee.Add(emp);
                    }
                    con.Close();
                }
                return lstEmployee;
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<TotalDays> GetTotalDays(int Empid, int WeekID)
        {
            try
            {
                List<TotalDays> lsttotaldays = new List<TotalDays>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_GetTotalTaskEmployeeIDAndWeedID", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@EmpID", Empid));
                    cmd.Parameters.Add(new SqlParameter("@WeekID", WeekID));
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        TotalDays td = new TotalDays();

                        td.TotalSunday = Convert.ToInt32(rdr["TotalSunday"]);
                        td.TotalMonday = Convert.ToInt32(rdr["TotalMonday"]);
                        td.TotalTuesday = Convert.ToInt32(rdr["TotalTuesday"]);
                        td.TotalWednesday = Convert.ToInt32(rdr["TotalWednesday"]);
                        td.TotalThrusday = Convert.ToInt32(rdr["TotalThrusday"]);

                        td.TotalFriday = Convert.ToInt32(rdr["TotalFriday"]);
                        td.TotalSaturday = Convert.ToInt32(rdr["TotalSaturday"]);


                        lsttotaldays.Add(td);
                    }
                    con.Close();
                }
                return lsttotaldays;
            }
            catch
            {
                throw;
            }
        }

        public int AddTimeTables (List<EmpTaskTran> Listobjemptasktran, int EmpID,int WeekID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("Sp_InsertIntoEmpTaskTran", con);
                    cmd.CommandType = CommandType.StoredProcedure;           
                    cmd.Parameters.AddWithValue("@EmpID", EmpID);
                    cmd.Parameters.AddWithValue("@WeekId", WeekID);
                    foreach (var i in Listobjemptasktran)
                    {
                        cmd.Parameters.AddWithValue("@TaskID", i.TaskId);
                        cmd.Parameters.AddWithValue("@Sunday", i.Sunday);
                        cmd.Parameters.AddWithValue("@Monday", i.Monday);
                        cmd.Parameters.AddWithValue("@Tuesday", i.Tuesday);
                        cmd.Parameters.AddWithValue("@Wednesday", i.Wednesday);
                        cmd.Parameters.AddWithValue("@Thrusday", i.Thrusday);
                        cmd.Parameters.AddWithValue("@Friday", i.Friday);
                        cmd.Parameters.AddWithValue("@Saturday", i.Saturday);
                    }
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }




    }
}
