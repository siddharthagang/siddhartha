﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class EmpTaskTran
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int EmpTaskId { get; set; }
        [Required]
        public int EmpID { get; set; }
        [Required]
        public int WeekId { get; set; }
        [Required]
        public int TaskId { get; set; }

        public string  TaskName { get; set; }

        public int  Sunday { get; set; }
        
        public int  Monday { get; set; }
       
        public int  Tuesday { get; set; }
         public int  Wednesday { get; set; }
         public int  Thrusday { get; set; }
         public int  Friday { get; set; }
         public int  Saturday { get; set; }
    }
}
