﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class RootObject
    {
        public int userid { get; set; }
        public string weekid { get; set; }
       public List<EmpTaskTran> tasks { get; set; }
    }
}
