﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class TotalDays
    {
        public int TotalSunday { get; set; }
        public int TotalMonday { get; set; }
        public int TotalTuesday { get; set; }
        public int TotalWednesday { get; set; }
        public int TotalThrusday { get; set; }
        public int TotalFriday { get; set; }
        public int TotalSaturday { get; set; }
    }
}
