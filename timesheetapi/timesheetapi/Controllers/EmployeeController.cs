﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.data;
using timesheet.model;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace timesheetapi.Controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : Controller
    {
        TimeSheetDBDataAccess objtimeSheetDBData = new TimeSheetDBDataAccess();
        private readonly EmployeeService employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        //[HttpGet("getall")]
        //public IActionResult GetAll(string text)
        //{
        //    var items = this.employeeService.GetEmployees();
        //    return new ObjectResult(items);
        //}

        [HttpGet]
        public IEnumerable<Employee> GetAll()
        {
            return objtimeSheetDBData.GetAllEmployee();
        }
    }
}
