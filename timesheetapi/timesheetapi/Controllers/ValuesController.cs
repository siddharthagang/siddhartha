﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using timesheet.business;
using timesheet.data;
using timesheet.model;

namespace timesheetapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        TimeSheetDBDataAccess objtimeSheetDBData = new TimeSheetDBDataAccess();
        // GET api/values

        [HttpGet("get")]
        //[HttpGet("Create/{id}")]
        public IEnumerable<Employee> Get()
        {
            return objtimeSheetDBData.GetAllEmployee();
        }
      //  call when this  project load
      
        [HttpGet("getalltotal")]
        public IEnumerable<Employee> GetAllTimes()
        {
            return objtimeSheetDBData.GetAllTimes();
        }

        
        [HttpGet("gettaskwithtime/{empid}/{weekid}")]
        public IEnumerable<EmpTaskTran> GetTaskWithTime(int Empid, int WeekID)
        {
            return objtimeSheetDBData.GetTaskWithTime(Empid, WeekID);
        }

        // GET api/values/5
        [HttpGet("getemployeebyid{id}")]
        public Employee GetEmployeeByID(int id)
        {
            return objtimeSheetDBData.GetEmployeeByID(id);
        }

        [HttpGet("gettotaldays/{empid}/{weekid}")]
        public IEnumerable<TotalDays> GetTotaldays(int Empid, int WeekID)
        {
            return objtimeSheetDBData.GetTotalDays(Empid, WeekID);
        }
      

        // POST api/values
        [HttpPost]
        public int Post([FromBody]JObject jsonData)

        {
            int empid = 0;
            int weekid = 0;
            RootObject objtasks = null;
            List<EmpTaskTran> listvalue = null;
            objtasks = jsonData.ToObject<RootObject>();
            empid = objtasks.userid;
            weekid = Convert.ToInt32(objtasks.weekid);
            if(objtasks.tasks!=null && objtasks.tasks.Count()> 0)
            listvalue = objtasks.tasks;
            return objtimeSheetDBData.AddTimeTables(listvalue,empid,weekid);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
